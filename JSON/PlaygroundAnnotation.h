//
//  PlaygroundAnnotation.h
//  Memm
//
//  Created by Andri Thorhallsson on 06/08/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mapkit/Mapkit.h>
#import "Playground.h"
#import "PlaygroundInfoViewController.h"

@interface PlaygroundAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D cooridnate;
@property (nonatomic, retain) NSString *playgroundId;
@property (nonatomic, retain) NSString *imageName;
@property (copy, nonatomic) NSString *title;

- (id)initWithPlayground:(Playground *)playground;
- (MKAnnotationView *)annotationView;

@end
