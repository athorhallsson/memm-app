//
//  FeedTableViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "FeedTableViewController.h"
#import "CredentialStore.h"

@interface FeedTableViewController ()

@end

@implementation FeedTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"FeedTableViewCellNib" bundle:nil] forCellReuseIdentifier:@"FeedCellIdentifier"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchFeed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [feed count];
}

-(void)fetchFeed {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/rpcapi/rpc/Feed"]]];
    [request setHTTPMethod:@"Post"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *allPosts = [NSJSONSerialization
                                       JSONObjectWithData:requestHandler
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        feed = [[NSMutableArray alloc] init];
        for ( NSDictionary *thePost in allPosts ) {
            Post *newPost = [[Post alloc] initWithDictionary:thePost];
            [feed addObject:newPost];
        }
    [[self tableView] reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedCell *cell = (FeedCell *)[self.tableView dequeueReusableCellWithIdentifier:@"FeedCellIdentifier"];

    Post *post = [feed objectAtIndex:indexPath.row];
    [self configureCell:cell forPost:post];
    
    return cell;
}

- (void)configureCell:(FeedCell *)cell forPost:(Post *)post {
    NSURL *myUrl = [NSURL URLWithString:post.author.imagePath];
    [self downloadImageWithURL:myUrl completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
            cell.imageView.clipsToBounds = YES;
            cell.imageView.image = image;
        }
    }];
    
    UIView *whiteRoundedCornerView = [[UIView alloc] initWithFrame:CGRectMake(5 , 10, self.view.bounds.size.width-10, 120)];
     whiteRoundedCornerView.backgroundColor = [UIColor memmBeige];
    
    whiteRoundedCornerView.layer.shadowOpacity = 1.55;
    whiteRoundedCornerView.layer.shadowColor = (__bridge CGColorRef)([UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f]);
    whiteRoundedCornerView.layer.cornerRadius = 10.0;
    whiteRoundedCornerView.layer.shadowOffset = CGSizeMake(-1, -1);
    whiteRoundedCornerView.layer.shadowOpacity = 0.5;
    [cell.contentView addSubview:whiteRoundedCornerView];
    [cell.contentView sendSubviewToBack:whiteRoundedCornerView];

    [cell.authorLabel setText:post.author.name];
    [cell.timeLabel setText:post.dateCreated];
    [cell.textView setText:post.text];
    [cell.numberOfCommentsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)post.comments.count]];
    cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
    [cell.imageView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [cell.imageView.layer setBorderWidth: 3.0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               } else{
                                   completionBlock(NO, nil);
                               }
                           }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Post *post = [feed objectAtIndex:indexPath.row];

    [self performSegueWithIdentifier:@"showPostDetails" sender:post];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue destinationViewController] isKindOfClass:[PostDetailViewController class]]) {
        [[segue destinationViewController] setPost:sender];
    }
}

@end
