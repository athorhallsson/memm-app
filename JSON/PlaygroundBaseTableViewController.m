//
//  PlaygroundBaseTableViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 26/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PlaygroundBaseTableViewController.h"
#import "Playground.h"

NSString *const kCellIdentifier2 = @"PlaygroundCell";


@interface PlaygroundBaseTableViewController ()

@end

@implementation PlaygroundBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureCell:(UITableViewCell *)cell forPlayground:(Playground *)playground {
    [[cell textLabel] setText:[playground name]];
    if ([playground currentUserIsCheckedIn]) {
        UIImage *tempImage = [UIImage imageNamed:@"LocationWhite"];
        [cell.imageView setImage:tempImage];
    }
    else if ([playground activeFriends]) {
        UIImage *tempImage = [UIImage imageNamed:@"LocationRingGreen"];
        [cell.imageView setImage:tempImage];
        
    }
    else if (![playground empty]) {
        UIImage *tempImage = [UIImage imageNamed:@"LocationRingOrange"];
        [cell.imageView setImage:tempImage];
        
    }
    else {
        UIImage *tempImage = [UIImage imageNamed:@"LocationRingRed"];
        [cell.imageView setImage:tempImage];
        
    }
    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%d m",(int)[playground range]]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
