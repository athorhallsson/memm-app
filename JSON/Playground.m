//
//  Playground.m
//  Memm
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "Playground.h"

@implementation Playground

- (id)initWithDictionary:(NSDictionary *)thePlayground {

    [self setName:thePlayground[@"Name"]];
    [self setRange:[thePlayground[@"Range"] intValue]];
    [self setPlaygroundId: thePlayground[@"Id"]];
    [self setActiveFriends:[thePlayground[@"ActiveFriends"]  boolValue]];
    [self setEmpty:[thePlayground[@"Empty"] boolValue]];
    [self setCurrentUserIsCheckedIn:[thePlayground[@"CurrentUserIsCheckedIn"] boolValue]];
    [self setLat:[NSNumber numberWithDouble:[thePlayground[@"Latitude"] doubleValue]]];
    [self setLon:[NSNumber numberWithDouble:[thePlayground[@"Longitude"] doubleValue]]];
    
    return self;
}

@end
