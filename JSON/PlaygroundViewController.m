//
//  PlaygroundViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 24/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PlaygroundViewController.h"

#define KILOMETER 1000

@interface PlaygroundViewController ()
@end

@implementation PlaygroundViewController
@synthesize playgroundId;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchPlayground];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Guest access not in use ATM
- (void) fetchPlayground
{
    NSString *post = [NSString stringWithFormat:@"id=%@", playgroundId];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://webservice.memmapp.com/Home/PlaygroundInfoJson"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    NSError *error;
    NSMutableDictionary *playground = [NSJSONSerialization
                                       JSONObjectWithData:requestHandler
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        // Name
         [_nameLabel setText:playground[@"Name"]];
        
        // Rating
        [_ratingLabel setText:[NSString stringWithFormat:@"%@",playground[@"Rating"]]];
        
        // Map
        CLLocationCoordinate2D zoomLocation;
        zoomLocation.latitude = [playground[@"Latitude"] doubleValue];
        zoomLocation.longitude= [playground[@"Longitude"] doubleValue];
        
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.8*KILOMETER, 0.8*KILOMETER);
        [_mapView setRegion:viewRegion animated:YES];
        
        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = zoomLocation;
        point.title = playground[@"Name"];
        point.subtitle = @"Playground";
        
        [_mapView addAnnotation:point];
        
        // Image
        NSMutableDictionary *pImage = playground[@"PlaygroundImage"];
        NSString *path = pImage[@"Path"];
        NSString *host = @"http://memmapp.com";
        NSString *filePath = [host stringByAppendingString:path];
        NSURL *myUrl = [NSURL URLWithString:filePath];
        [self downloadImageWithURL:myUrl completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                // change the image in the cell
                _imageView.image = image;
            }
        }];
        
    }
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               } else{
                                   completionBlock(NO, nil);
                               }
                           }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
