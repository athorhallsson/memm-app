//
//  PlaygroundResultsTableViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 26/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaygroundBaseTableViewController.h"
#import "Playground.h"

@interface PlaygroundResultsTableViewController : PlaygroundBaseTableViewController

@property (nonatomic, strong) NSArray *filteredPlaygrounds;
@property (nonatomic, strong) NSArray *headers;

@end
