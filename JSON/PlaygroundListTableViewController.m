//
//  PlaygroundListTableViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PlaygroundListTableViewController.h"


@interface PlaygroundListTableViewController () <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController *searchController;

// our secondary search results table view
@property (nonatomic, strong) PlaygroundResultsTableViewController *resultsTableController;

// for state restoration
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;
@end

@implementation PlaygroundListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _resultsTableController = [[PlaygroundResultsTableViewController alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
    
    // Set the background color of the results table to Green
    [self.resultsTableController.tableView setBackgroundColor:[UIColor colorWithRed:171.0/255 green:189.0/255 blue:167.0/255 alpha:1]];
    [self.resultsTableController.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchPlaygrounds];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sections count];
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    return [headers objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[sections objectAtIndex:section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier2];
    
    Playground *playground = [[sections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self configureCell:cell forPlayground:playground];
    
    return cell;
}

- (void)fetchPlaygrounds {
    [self getMyPlaygrounds];
    [self getNearbyPlaygrounds];
    
    headers = [NSArray arrayWithObjects:@"My Playgrounds",@"Nearby Playgrounds", nil];
    sections = [NSArray arrayWithObjects:myPlaygrounds, nearbyPlaygrounds, nil];

    [self.tableView reloadData];
}

- (void)getMyPlaygrounds {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://test.memmapp.com/rpcapi/rpc/MyPlaygrounds"]];
    [request setHTTPMethod:@"Post"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *myPlaygroundsDict = [NSJSONSerialization
                                              JSONObjectWithData:requestHandler
                                              options:NSJSONReadingMutableContainers
                                              error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        myPlaygrounds = [[NSMutableArray alloc] init];
        
        for ( NSDictionary *thePlayground in myPlaygroundsDict )
        {
            [myPlaygrounds addObject:[[Playground alloc] initWithDictionary:thePlayground]];
        }
    }

}

- (void)getNearbyPlaygrounds {
    nearbyPlaygrounds = [[NSMutableArray alloc] init];
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://test.memmapp.com/rpcapi/rpc/NearbyPlaygroundsUser?lat=66&lon=21"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    NSError *error;
    NSMutableDictionary *allPlaygrounds = [NSJSONSerialization
                                           JSONObjectWithData:requestHandler
                                           options:NSJSONReadingMutableContainers
                                           error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        for ( NSDictionary *thePlayground in allPlaygrounds )
        {
            [nearbyPlaygrounds addObject:[[Playground alloc] initWithDictionary:thePlayground]];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        NSString *playgroundId = [[[sections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] playgroundId];
        [self performSegueWithIdentifier:@"showPlaygroundInfo" sender:playgroundId];
    }
    else {
        PlaygroundResultsTableViewController *tableController = (PlaygroundResultsTableViewController *)self.searchController.searchResultsController;
        
        NSString *playgroundId = [[[tableController.filteredPlaygrounds objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] playgroundId];
        [self performSegueWithIdentifier:@"showPlaygroundInfo" sender:playgroundId];
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    
    if (searchText.length < 1) {
        return;
    }
    
    // get the relevant Playgrounds
    CredentialStore *store = [[CredentialStore alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    // TODO Get users location
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/UserSearchPlaygrounds?search=%@&lat=66&lon=21", searchText]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"Post"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSArray *results = [NSJSONSerialization
                       JSONObjectWithData:requestHandler
                       options:NSJSONReadingMutableContainers
                       error:&error];
    
    NSMutableArray *filteredMyPlaygrounds = [[NSMutableArray alloc] init];
    NSMutableArray *filteredNearbyPlaygrounds = [[NSMutableArray alloc] init];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        NSMutableDictionary *myPlaygroundsDict = results[0];
        NSMutableDictionary *nearbyPlaygroundsDict = results[1];
        
        for ( NSDictionary *thePlayground in myPlaygroundsDict ) {
            [filteredMyPlaygrounds addObject:[[Playground alloc] initWithDictionary:thePlayground]];
        }
        
        for ( NSDictionary *thePlayground in nearbyPlaygroundsDict ) {
            [filteredNearbyPlaygrounds addObject:[[Playground alloc] initWithDictionary:thePlayground]];
        }
    }
    
    // hand over the filtered results to our search results table
    NSMutableArray *searchResults = [NSMutableArray arrayWithObjects:filteredMyPlaygrounds, filteredNearbyPlaygrounds, nil];

    PlaygroundResultsTableViewController *tableController = (PlaygroundResultsTableViewController *)self.searchController.searchResultsController;
    tableController.filteredPlaygrounds = searchResults;
    [tableController.tableView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender) {
        [[segue destinationViewController] setPlaygroundId:sender];
    }
}


@end
