//
//  ViewController.h
//  JSON
//
//  Created by Andri Thorhallsson on 21/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UITableViewController {
    NSMutableArray *items;
}

- (void)fetchEntries;

@end

