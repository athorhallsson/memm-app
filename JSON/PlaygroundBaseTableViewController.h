//
//  PlaygroundBaseTableViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 26/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>



@class Playground;
extern NSString *const kCellIdentifier2;

@interface PlaygroundBaseTableViewController : UITableViewController

- (void)configureCell:(UITableViewCell *)cell forPlayground:(Playground *)playground;



@end
