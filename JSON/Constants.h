//
//  Constants.h
//  Memm
//
//  Created by Andri Thorhallsson on 29/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Constants : NSObject

extern NSString* const HOST;

@end

@interface UIColor (Memm)

+(UIColor *) memmGreen;
+(UIColor *) memmBeige;
+(UIColor *) memmBlue;
+(UIColor *) memmYellow;
+(UIColor *) memmOrange;
+(UIColor *) memmRed;
+(UIColor *) memmDarkGreen;

@end

@implementation UIColor (Memm)

+(UIColor *) memmGreen { return [UIColor colorWithRed:183/255.0 green:196/255.0 blue:170/255.0 alpha:1.0]; }
+(UIColor *) memmBeige { return [UIColor colorWithRed:229/255.0 green:222/255.0 blue:202/255.0 alpha:1.0]; }
+(UIColor *) memmBlue { return [UIColor colorWithRed:129/255.0 green:139/255.0 blue:151/255.0 alpha:1.0]; }
+(UIColor *) memmYellow { return [UIColor colorWithRed:215/255.0 green:179/255.0 blue:56/255.0 alpha:1.0]; }
+(UIColor *) memmOrange { return [UIColor colorWithRed:204/255.0 green:111/255.0 blue:43/255.0 alpha:1.0]; }
+(UIColor *) memmRed { return [UIColor colorWithRed:116/255.0 green:53/255.0 blue:24/255.0 alpha:1.0]; }
+(UIColor *) memmDarkGreen { return [UIColor colorWithRed:115/255.0 green:111/255.0 blue:67/255.0 alpha:1.0]; }

@end