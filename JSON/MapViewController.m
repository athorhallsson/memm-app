//
//  MapViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 23/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "MapViewController.h"
#import "CredentialStore.h"
#import "Playground.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager.delegate = self;
    self.mapView.delegate = self;
    [self getNearbyPlaygrounds];
    
    _manager = [[CLLocationManager alloc] init];
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusNotDetermined) {
        [_manager requestWhenInUseAuthorization];
    }
    else if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [self.manager startUpdatingLocation];
        _mapView.showsUserLocation = YES;
        // Set map to user location
        CLLocationCoordinate2D userLocation = self.manager.location.coordinate;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation, 2000, 2000);
        [self.mapView setRegion:region animated:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location" message:@"Please allow location access under settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.manager startUpdatingLocation];
        _mapView.showsUserLocation = YES;
        // Set map to user location
        CLLocationCoordinate2D userLocation = self.manager.location.coordinate;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation, 2000, 2000);
        [self.mapView setRegion:region animated:YES];
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    // use the annotation view as the sender
    [self performSegueWithIdentifier:@"playgroundInfo" sender:view];
}

- (void)getNearbyPlaygrounds {
    nearbyPlaygrounds = [[NSMutableArray alloc] init];
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/rpcapi/rpc/NearbyPlaygroundsUser?lat=66&lon=21"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    NSError *error;
    NSMutableDictionary *allPlaygrounds = [NSJSONSerialization
                                           JSONObjectWithData:requestHandler
                                           options:NSJSONReadingMutableContainers
                                           error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        for ( NSDictionary *thePlayground in allPlaygrounds )
        {
            [nearbyPlaygrounds addObject:[[Playground alloc] initWithDictionary:thePlayground]];
        }
        [self updateMap];
    }
}

- (void)updateMap {
    for (Playground *thePlayground in nearbyPlaygrounds) {
        PlaygroundAnnotation *point = [[PlaygroundAnnotation alloc] initWithPlayground:thePlayground];
        [_mapView addAnnotation:point];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[PlaygroundAnnotation class]]) {
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"PlaygroundAnnotationId"];
        
        PlaygroundAnnotation *myLocation = (PlaygroundAnnotation *)annotation;
        
        if (annotationView == nil) {
            annotationView = myLocation.annotationView;
        }
        else {
            annotationView.annotation = annotation;
        }
        return annotationView;
    }
    return nil;
}


#pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(MKAnnotationView *)sender
{
    if ([segue.identifier isEqualToString:@"playgroundInfo"]) {
         PlaygroundAnnotation *myAnnotation = (PlaygroundAnnotation *)sender.annotation;
         [[segue destinationViewController] setPlaygroundId:[NSString stringWithFormat:@"%@", myAnnotation.playgroundId]];
    }
}


@end
