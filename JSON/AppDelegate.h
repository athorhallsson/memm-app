//
//  AppDelegate.h
//  JSON
//
//  Created by Andri Thorhallsson on 21/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CredentialStore.h"
#import "Constants.h"
#import "LoginViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end

