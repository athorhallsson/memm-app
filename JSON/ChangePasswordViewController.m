//
//  ChangePasswordViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 23/08/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirmPressed {
    CredentialStore *store = [[CredentialStore alloc] init];
    NSDictionary* jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [_oldPasswordTextField text],
                              @"OldPassword",
                              self.updatedPasswordTextField.text,
                              @"NewPassword",
                              self.confirmUpdatedPasswordTextField.text,
                              @"ConfirmPassword",
                              nil];
    
    //convert object to data
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/api/Account/ChangePassword"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:HOST forHTTPHeaderField:@"Referer"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:nil];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) requestResponse;
    
    NSLog(@"%@", requestResponse);
    
    if ((long)[httpResponse statusCode] == 200)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ups" message:@"Update failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
