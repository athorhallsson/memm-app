//
//  PostViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 24/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface PostViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *postTextField;

@end
