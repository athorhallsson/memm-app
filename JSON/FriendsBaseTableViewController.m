//
//  FriendsBaseViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 20/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "FriendsBaseTableViewController.h"
#import "User.h"

@interface FriendsBaseTableViewController ()

@end

NSString *const kCellIdentifier = @"FriendCellId";
NSString *const kTableCellNibName = @"FriendCell";

@implementation FriendsBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self.tableView registerNib:[UINib nibWithNibName:kTableCellNibName bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureCell:(UITableViewCell *)cell forUser:(User *)user {
    cell.textLabel.text = user.name;
    cell.detailTextLabel.text = user.activePlayground;
    NSURL *myUrl = [NSURL URLWithString:user.imagePath];
    NSData *imageData = [NSData dataWithContentsOfURL:myUrl];
    UIImage *image = [UIImage imageWithData:imageData];
    [cell.imageView setImage:image];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
