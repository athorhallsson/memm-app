//
//  ViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 21/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "ListViewController.h"
#import "GuestPlayground.h"
#import "PlaygroundViewController.h"
#import "CredentialStore.h"

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CredentialStore *store = [[CredentialStore alloc] init];
    if ([store isLoggedIn]) {
        [self performSegueWithIdentifier:@"loggedIn" sender:nil];
    }
    [self fetchEntries];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"UITableViewCell"];
    }
    GuestPlayground *item = [items objectAtIndex:[indexPath row]];
    [[cell textLabel] setText:[item name]];
   [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%d m",(int)[item range]]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [items count];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self fetchEntries];
    }
    return self;
}

- (void) fetchEntries
{
    // Create a new data container for the stuff tha comes back from the service
    items = [[NSMutableArray alloc] init];
    
    NSString *post = [NSString stringWithFormat:@"lat=66&lon=21"];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://memmapp.com/Home/GuestPlaygroundListJson"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    NSError *error;
    NSLog(@"%@", requestResponse);
    NSMutableDictionary *allPlaygrounds = [NSJSONSerialization
                                       JSONObjectWithData:requestHandler
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        for ( NSDictionary *thePlayground in allPlaygrounds )
        {
            GuestPlayground *newPlayground = [[GuestPlayground alloc] init];
            [newPlayground setName:thePlayground[@"Name"]];
            NSInteger temp = [thePlayground[@"Range"] intValue];
            [newPlayground setRange:&temp];
            [newPlayground setPlaygroundId: thePlayground[@"Id"]];
            
            [items addObject:newPlayground];
        }
    }
    [[self tableView] reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PlaygroundViewController *playgroundViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaygroundViewController"];
    
    NSString *pId = [NSString stringWithFormat:@"%@", [[items objectAtIndex:indexPath.row] playgroundId]];
    
    playgroundViewController.playgroundId = pId;
    [self performSegueWithIdentifier:@"showNearbyPlaygroundDetail" sender:pId];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if (sender) {
         [[segue destinationViewController] setPlaygroundId:sender];
     }
     
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
 }



@end
