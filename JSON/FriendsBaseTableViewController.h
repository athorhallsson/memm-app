//
//  FriendsBaseViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 20/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;
extern NSString *const kCellIdentifier;

@interface FriendsBaseTableViewController : UITableViewController

- (void)configureCell:(UITableViewCell *)cell forUser:(User *)user;

@end
