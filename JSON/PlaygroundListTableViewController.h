//
//  PlaygroundListTableViewController.h
//  JSON
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaygroundBaseTableViewController.h"
#import "CredentialStore.h"
#import "Playground.h"
#import "PlaygroundInfoViewController.h"
#import "PlaygroundResultsTableViewController.h"

@interface PlaygroundListTableViewController : PlaygroundBaseTableViewController {
    NSArray* headers;
    NSArray* sections;
    NSMutableArray *myPlaygrounds;
    NSMutableArray *nearbyPlaygrounds;
}

- (void)fetchPlaygrounds;
- (void)getNearbyPlaygrounds;

@end
