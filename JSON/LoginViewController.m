//
//  LoginViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 30/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)confirmPressed:(id)sender {
    NSString *post = [NSString stringWithFormat:@"grant_type=password&username=%@&password=%@",
                      [self.userNameTextField text], [self.passwordTextField text]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/token"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:HOST forHTTPHeaderField:@"Referer"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *jsonResponse = [NSJSONSerialization
                                       JSONObjectWithData:requestHandler
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        NSLog(@"%@", jsonResponse);
    }
    else {
        if (jsonResponse[@"access_token"]) {
            CredentialStore *store = [[CredentialStore alloc] init];
            [store setAuthToken:jsonResponse[@"access_token"]];
            
            [self performSegueWithIdentifier:@"loginConfirmed" sender:self];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Invalid authentication." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
