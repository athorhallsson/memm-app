//
//  PlaygroundViewController.h
//  JSON
//
//  Created by Andri Thorhallsson on 24/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface PlaygroundViewController : UIViewController

@property (nonatomic, retain) NSString *playgroundId;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (void) fetchPlayground;

@end
