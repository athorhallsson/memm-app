//
//  MeViewController.h
//  JSON
//
//  Created by Andri Thorhallsson on 07/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CredentialStore.h"
#import "DateTools.h"
#import "Constants.h"

@interface MeViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *infoBackground;

- (void)uploadImage:(UIImage *)image;

@end
