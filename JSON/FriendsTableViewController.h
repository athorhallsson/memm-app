//
//  FriendsTableViewController.h
//  JSON
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsBaseTableViewController.h"

@interface FriendsTableViewController : FriendsBaseTableViewController {
    NSMutableArray *requests;
    NSMutableArray *friends;
    NSMutableArray *strangers;
    NSArray* headers;
    NSArray* sections;
}

- (void)fetchFriends;

@end
