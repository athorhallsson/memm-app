//
//  ChangePasswordViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 23/08/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "CredentialStore.h"

@interface ChangePasswordViewController : UIViewController <UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *updatedPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmUpdatedPasswordTextField;

@end
